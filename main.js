const startButton = document.getElementById("start");
const stopButton = document.getElementById("stop");
const downloadButton = document.getElementById("download");

// Initially disable the download button
downloadButton.disabled = true;

let mediaRecorder;
let recordedChunks = [];
let recordingStartTime;
let recordingInterval;
let statusElement = document.getElementById("status");

startButton.addEventListener("click", async () => {
  // Disable the start button once recording has started
  startButton.disabled = true;
  stopButton.disabled = false;
  try {
    if (!navigator.mediaDevices || !navigator.mediaDevices.getDisplayMedia) {
      throw new Error("Screen recording is not supported in your browser");
    }
    const stream = await navigator.mediaDevices.getDisplayMedia({
      video: true,
    });
    mediaRecorder = new MediaRecorder(stream);

    mediaRecorder.ondataavailable = (e) => {
      if (e.data.size > 0) {
        recordedChunks.push(e.data);
      }
    };

    mediaRecorder.start();
    console.log("Recording has started");
  } catch (error) {
    if (error.name === "NotAllowedError") {
      document.getElementById("error").textContent =
        "Screen recording permission was denied. Please allow screen recording to use this feature.";
    } else {
      document.getElementById("error").textContent =
        "Failed to start recording: " + error;
    }
  }
});

stopButton.addEventListener("click", () => {
  mediaRecorder.stop();
  // Enable the download button after recording has stopped
  downloadButton.disabled = false;
});

downloadButton.addEventListener("click", () => {
  if (recordedChunks.length > 0) {
    try {
      const blob = new Blob(recordedChunks, {
        type: "video/webm",
      });

      const url = URL.createObjectURL(blob);
      const a = document.createElement("a");
      a.href = url;
      a.download = "recorded.webm";
      a.click();
    } catch (error) {
      document.getElementById("error").textContent =
        "Download failed: " + error;
    }
  } else {
    console.log("No recorded chunks available for download");
  }
});
